CREATE DATABASE  IF NOT EXISTS `unicart` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `unicart`;
-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: unicart
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ima`
--

DROP TABLE IF EXISTS `ima`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ima` (
  `idKorisnika` varchar(45) NOT NULL,
  `idLista` int NOT NULL,
  PRIMARY KEY (`idKorisnika`,`idLista`),
  KEY `idLista_idx` (`idLista`),
  CONSTRAINT `idKorisnika` FOREIGN KEY (`idKorisnika`) REFERENCES `korisnik` (`username`),
  CONSTRAINT `idLista` FOREIGN KEY (`idLista`) REFERENCES `lista` (`idLista`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ima`
--

LOCK TABLES `ima` WRITE;
/*!40000 ALTER TABLE `ima` DISABLE KEYS */;
/*!40000 ALTER TABLE `ima` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `istorijakupovina`
--

DROP TABLE IF EXISTS `istorijakupovina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `istorijakupovina` (
  `idKupovine` int NOT NULL AUTO_INCREMENT,
  `kupac` varchar(45) NOT NULL,
  `iznosRacuna` int NOT NULL,
  `datum` datetime NOT NULL,
  `idListe` int NOT NULL,
  PRIMARY KEY (`idKupovine`),
  KEY `kupac_idx` (`kupac`),
  KEY `idLista_idx` (`idListe`),
  CONSTRAINT `idListe` FOREIGN KEY (`idListe`) REFERENCES `lista` (`idLista`) ON UPDATE CASCADE,
  CONSTRAINT `kupac` FOREIGN KEY (`kupac`) REFERENCES `korisnik` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `istorijakupovina`
--

LOCK TABLES `istorijakupovina` WRITE;
/*!40000 ALTER TABLE `istorijakupovina` DISABLE KEYS */;
/*!40000 ALTER TABLE `istorijakupovina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `korisnik`
--

DROP TABLE IF EXISTS `korisnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `korisnik` (
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `ime` varchar(45) NOT NULL,
  `prezime` varchar(45) NOT NULL,
  `datumRodjenja` datetime NOT NULL,
  `brTelefona` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `rola` varchar(45) NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `brTelefona_UNIQUE` (`brTelefona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `korisnik`
--

LOCK TABLES `korisnik` WRITE;
/*!40000 ALTER TABLE `korisnik` DISABLE KEYS */;
/*!40000 ALTER TABLE `korisnik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lista`
--

DROP TABLE IF EXISTS `lista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lista` (
  `idLista` int NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `privatnost` varchar(45) NOT NULL,
  `kod` varchar(45) DEFAULT NULL,
  `budzet` int DEFAULT NULL,
  `usernameKorisnka` varchar(45) NOT NULL,
  PRIMARY KEY (`idLista`),
  UNIQUE KEY `kod_UNIQUE` (`kod`),
  KEY `usernameKorisnik_idx` (`usernameKorisnka`),
  CONSTRAINT `usernameKorisnik` FOREIGN KEY (`usernameKorisnka`) REFERENCES `korisnik` (`username`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lista`
--

LOCK TABLES `lista` WRITE;
/*!40000 ALTER TABLE `lista` DISABLE KEYS */;
/*!40000 ALTER TABLE `lista` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `namirnice`
--

DROP TABLE IF EXISTS `namirnice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `namirnice` (
  `idNamirnice` int NOT NULL AUTO_INCREMENT,
  `kategorija` varchar(45) NOT NULL,
  `proizvod` varchar(45) NOT NULL,
  `idListe` int NOT NULL,
  PRIMARY KEY (`idNamirnice`),
  KEY `idListe_idx` (`idListe`),
  CONSTRAINT `idList` FOREIGN KEY (`idListe`) REFERENCES `lista` (`idLista`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `namirnice`
--

LOCK TABLES `namirnice` WRITE;
/*!40000 ALTER TABLE `namirnice` DISABLE KEYS */;
/*!40000 ALTER TABLE `namirnice` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-27  0:08:33
