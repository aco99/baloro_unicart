-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 06, 2021 at 08:39 PM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `unicart`
--

-- --------------------------------------------------------

--
-- Table structure for table `ima`
--

DROP TABLE IF EXISTS `ima`;
CREATE TABLE IF NOT EXISTS `ima` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idKorisnika` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `idLista` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idLista_idx` (`idLista`),
  KEY `idKorisnika` (`idKorisnika`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `ima`
--

INSERT INTO `ima` (`id`, `idKorisnika`, `idLista`) VALUES
(8, 'aco123', 15),
(9, 'ivana123', 15),
(21, 'aco123', 18),
(24, 'ana123', 22),
(37, 'ivana123', 35),
(41, 'ivana123', 22),
(42, 'aco123', 36),
(46, 'ana123', 40);

-- --------------------------------------------------------

--
-- Table structure for table `istorijakupovina`
--

DROP TABLE IF EXISTS `istorijakupovina`;
CREATE TABLE IF NOT EXISTS `istorijakupovina` (
  `idKupovine` int(11) NOT NULL AUTO_INCREMENT,
  `kupac` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `iznosRacuna` int(11) NOT NULL,
  `datum` datetime NOT NULL,
  `idListe` int(11) NOT NULL,
  PRIMARY KEY (`idKupovine`),
  KEY `kupac_idx` (`kupac`),
  KEY `idLista_idx` (`idListe`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `istorijakupovina`
--

INSERT INTO `istorijakupovina` (`idKupovine`, `kupac`, `iznosRacuna`, `datum`, `idListe`) VALUES
(6, 'aco123', 200, '2021-06-02 21:36:00', 15),
(11, 'milos123', 100, '2021-06-03 02:37:00', 18),
(13, 'milos123', 100, '2021-06-03 02:39:00', 18),
(14, 'ana123', 200, '2021-06-06 16:29:00', 40);

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

DROP TABLE IF EXISTS `korisnik`;
CREATE TABLE IF NOT EXISTS `korisnik` (
  `username` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `password` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `ime` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `prezime` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `datumRodjenja` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `brTelefona` varchar(45) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `rola` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `brTelefona_UNIQUE` (`brTelefona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `korisnik`
--

INSERT INTO `korisnik` (`username`, `password`, `ime`, `prezime`, `datumRodjenja`, `brTelefona`, `email`, `rola`) VALUES
('aco123', 'aco123', 'Aco', 'Avramovic', '1999-04-12', '063464924', 'acoavramovic@gmail.com', 'korisnik'),
('ana123', 'ana123', 'Ana', 'Avramovic', '1999-04-12', '888333222', 'anaavramovic@prava.com', 'admin'),
('igor123', 'igor123', 'Igor', 'Ricka', '2021-04-27', '555888444', 'igorkaric@gmail.com', 'moderator'),
('ivana123', 'ivana123', 'Ivana', 'Marjanovic', '2021-05-06', '666777888', 'ivanamarjanovic@gmail.com', 'korisnik'),
('slobo123', 'slobo123', 'Slobo', 'Avramovic', '1996-09-12', '032661408', 'slobodan@etf.rs', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `lista`
--

DROP TABLE IF EXISTS `lista`;
CREATE TABLE IF NOT EXISTS `lista` (
  `idLista` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `privatnost` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `kod` varchar(45) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `budzet` int(11) DEFAULT NULL,
  `usernameKorisnika` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`idLista`),
  UNIQUE KEY `kod_UNIQUE` (`kod`),
  KEY `usernameKorisnik_idx` (`usernameKorisnika`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `lista`
--

INSERT INTO `lista` (`idLista`, `naziv`, `privatnost`, `kod`, `budzet`, `usernameKorisnika`) VALUES
(15, 'Nova godina', 'privatna', 'YKd8Gsf6', 5000, 'aco123'),
(18, 'Rodjendan', 'privatna', 'sT23dHlh', 1300, 'milos123'),
(22, 'Vikend', 'privatna', 'NPYhfB09', 0, 'ana123'),
(35, 'proba', 'privatna', 'faBavO9T', 0, 'ivana123'),
(36, 'Slava', 'privatna', 'yXvhfO3k', 0, 'aco123'),
(40, 'Zurka', 'javna', '9TE3k2se', 1300, 'ana123');

-- --------------------------------------------------------

--
-- Table structure for table `namirnice`
--

DROP TABLE IF EXISTS `namirnice`;
CREATE TABLE IF NOT EXISTS `namirnice` (
  `idNamirnice` int(11) NOT NULL AUTO_INCREMENT,
  `kategorija` varchar(45) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `proizvod` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`idNamirnice`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `namirnice`
--

INSERT INTO `namirnice` (`idNamirnice`, `kategorija`, `proizvod`) VALUES
(12, 'voce', 'banana'),
(13, 'voce', 'jabuka'),
(14, 'povrce', 'krastavac'),
(15, 'povrce', 'paradajz'),
(16, 'meso', 'kobasica'),
(17, 'mlecni', 'sir'),
(18, 'voce', 'kruska'),
(19, 'hemija', 'sampon'),
(20, 'voce', 'sljiva'),
(21, NULL, 'kokos'),
(23, NULL, 'pica'),
(24, NULL, 'sveska'),
(25, NULL, 'hleb'),
(27, NULL, 'pivo'),
(28, NULL, 'case'),
(29, NULL, 'cips'),
(30, NULL, 'mleko'),
(31, NULL, 'pasteta'),
(32, NULL, 'ulje');

-- --------------------------------------------------------

--
-- Table structure for table `poseduje`
--

DROP TABLE IF EXISTS `poseduje`;
CREATE TABLE IF NOT EXISTS `poseduje` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idNamirnica` varchar(45) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `idLista` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idLista_idx` (`idLista`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `poseduje`
--

INSERT INTO `poseduje` (`id`, `idNamirnica`, `idLista`) VALUES
(31, '12', 18),
(32, '20', 18),
(33, '24', 18),
(34, '24', 18),
(38, '25', 22),
(39, '14', 22),
(41, '24', 15),
(42, '17', 15),
(44, '16', 15),
(50, '12', 15),
(51, '20', 15),
(52, '14', 40);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
