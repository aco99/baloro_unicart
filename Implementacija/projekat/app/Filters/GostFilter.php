<?php

namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

/**
 * Aco Avramovic
* GostFilter – klasa za zastitu da se bez logovanja ne predje u rolu korisnika
*
* @version 1.0
*/

class GostFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null){
        $session=session();
        if($session->has('korisnik'))
            return redirect()->to(site_url('Korisnik/pocetna'));
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null){
        // Do something here
    }
}