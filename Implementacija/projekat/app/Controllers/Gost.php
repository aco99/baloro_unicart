<?php

namespace App\Controllers;

use App\Models\KorisnikModel;
use App\Controllers\Korisnik;
use App\Controllers\Administrator;
use App\Controllers\Moderator;

/**
* GostController – klasa za logovanje i registraciju
*
* @version 1.0
*/

class Gost extends BaseController {

	protected function prikaz($page, $data) {  
		$data['controller']= 'Gost';
		echo view("stranice/$page",$data);
	}
        
    public function login($poruka=null){
        $this->prikaz("login", ['poruka'=>$poruka]);
    }

	public function singIn($poruka=null){
        $this->prikaz("signin", ['poruka'=>$poruka]);
    }

	/**
	 * Ivana Marjanovic
     * Metoda koja omogucava logovanje korisnika
     *
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
	public function loginSubmit(){
		if ($this->request->getVar('username') == "") {
			return $this->login('Polje korisnicko ime je prazno.');
		}
		if ($this->request->getVar('password') == "") {
			return $this->login('Polje lozinka je prazno.');
		}
		if($this->request->getVar('username') != "" && $this->request->getVar('password') != ""){
			$korisnik= new KorisnikModel();
			$logKorisnik=$korisnik->pretraga($this->request->getVar('username'));
			
			if ($logKorisnik==null) {
				return $this->login('Polje korisnicko ime je pogresno.');
			}
			$logKorisnik=$korisnik->pretraga($this->request->getVar('username'))[0];
			if ($logKorisnik->password != $this->request->getVar('password')) {
				return $this->login('Polje lozinka je pogresno.');
			} else {
				$this->session->set('korisnik', $logKorisnik);
				//if($logKorisnik->rola == "admin"){
				// 	$admin=new Administrator();
				///	return redirect()->to(site_url('Administrator/pocetna'));
				//}
				//if($logKorisnik->rola == "moderator"){
				//	$admin=new Administrator();
				//  return redirect()->to(site_url('Moderator/pocetna'));
				//}else {
				//$kor=new Korisnik();
				return redirect()->to(site_url('Korisnik/pocetna'));
			}
		}   
	} 
	
	/**
	 * Aco Avramovic
     * Metoda koja omogucava registraciju korisnika
     *
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
	public function registracija(){

		$username=$this->request->getVar('username');
		$lozinka=$this->request->getVar('password');
		$potLozinka=$this->request->getVar('potvrda');
		$ime=$this->request->getVar('ime');
		$prezime=$this->request->getVar('prezime');
		$datumRodjenja=$this->request->getVar('datumRodjenja');
		$brTelefona=$this->request->getVar('telefon');
		$email=$this->request->getVar('mejl');

		if(!$this->validate(['username'=>'required',
		'password'=>'required','potvrda'=>'required','ime'=>'required','prezime'=>'required',
		'datumRodjenja'=>'required','telefon'=>'required','mejl'=>'required'])){
			return $this->singIn('Niste uneli sva polja!');
		}	

		if(!preg_match("/^[_a-zA-Z0-9-]+$/", $username)) {
			return $this->singIn('Korisnicko ime nije u ispravnom formatu!');
		}
		if($lozinka != $potLozinka){
			return $this->singIn('Lozinke nisu iste!');
		}

		if(!preg_match("/^[A-Z]+[a-z]+$/", $ime)) {
			return $this->singIn('Ime nije u ispravnom formatu!');
		}

		if(!preg_match("/^[A-Z]+[a-z]+$/", $prezime)) {
			return $this->singIn('Prezime nije u ispravnom formatu!');
		}

		if(!preg_match("/^\d\d\d\d\d\d\d\d\d$/", $brTelefona)) {
			return $this->singIn('Telefonski broj nije u ispravnom formatu!');
		}

		if(!preg_match("/^\w+@[a-z]{1,5}\.[a-z]{1,5}$/", $email)) {
			return $this->singIn('Email nije u ispravnom formatu!');
		}

		$korisnik= new KorisnikModel();
		$logKorisnik=$korisnik->pretraga($this->request->getVar('username'));
		if($logKorisnik != null) {
			return $this->singIn('Korisnik vec postoji u bazi!');
		}

		$korisnik->insert([
			'username'=>$username,
			'password'=>$lozinka,
			'ime'=>$ime,
			'prezime'=>$prezime,
			'datumRodjenja'=>$datumRodjenja,
			'brTelefona'=>$brTelefona,
			'email'=>$email,
			'rola'=>"korisnik"
		]);
		return redirect()->to(site_url('Gost/login'));
	}
}
