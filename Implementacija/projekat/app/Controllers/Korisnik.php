<?php

namespace App\Controllers;

use App\Models\ImaModel;
use App\Models\KorisnikModel;
use App\Models\ListaModel;
use App\Models\NamirniceModel;
use App\Models\PosedujeModel;
use App\Models\IstorijaModel;

/**
* KorisnikController – klasa za realizovanje svih funkcionalnosti korisnika
*
* @version 1.0
*/

class Korisnik extends BaseController {
	
	/**
     * Metoda koja prikazuje stranice
     *
     *
     */
	protected function prikaz($page, $data) {  
		$data['controller']= 'Korisnik';
		$data['korisnik']=$this->session->get('korisnik');
		echo view("stranice/$page",$data);
	}

	/**
     * Prikaz pocetne stranice
     *
     */
	public function pocetna($poruka=null){
        $this->prikaz("pocetna", ['poruka'=>$poruka]);
    }

	/**
     * Prikaz stranice za pravljenje nove liste
     *
     */
	public function napraviListu($poruka=null) {
		$this->prikaz('NovaLista', ['poruka'=>$poruka]);
	}
//--------------------------------------------------------------

	/**
     * Metoda za prikaz stranice kojom moderator dodaje proizvode u bazu
     *
     */
	public function dodavanjePredloga($poruka=null){
		if($this->session->get('korisnik')->rola != "moderator") {
			return $this->pocetna();
		}
		$pomocnik=0;
		$poruka=null;
		if (isset($_SESSION['poruka'])) {
			if($_SESSION['poruka'] == "Proizvod je uspešno dodat.") {
				$poruka=$_SESSION['poruka'];
				$pomocnik=1;
				$_SESSION['poruka']=null;	
			}else if($_SESSION['poruka'] == "Polje proizvod je prazno.") {
				$poruka=$_SESSION['poruka'];
				$_SESSION['poruka']=null;
			}else if($_SESSION['poruka'] == "Polje kategorija je prazno.") {
				$poruka=$_SESSION['poruka'];
				$_SESSION['poruka']=null;
			}
		}
        $this->prikaz("dodavanjePredloga", ['poruka'=>$poruka, 'pomocnik'=>$pomocnik]);
    }

	/**
	 * Ivana Marjanovic
     * Metoda koja prikazuje stranicu kojom administrator pretrazuje korisnike
     *
     * @param $poruka - poruka u slucaju uspeha ili neuspeha; $korisnikUsername-rezultat pretrage po username
	 * $korisnikIme- rezultat pretrage po imenu, $korisnikPrezime- rezultat pretrage po prezimenu, $korisnikImePrezime- rezultat pretrage po imenu i prezimenu
     */
	public function pretragaKorisnika($poruka=null, $korisnikUsername=null, $korisnikIme=null, $korisnikPrezime=null, $korisnikImePrezime=null) {
        $poruka=null;
		if($this->session->get('korisnik')->rola != "admin") {
			return $this->pocetna($poruka);
		}
		$pomocnik=0;
		if (isset($_SESSION['poruka'])) {
			if($_SESSION['poruka'] == "Korisnik(ci) je(su) postavljen(i) za moderatora.") {
				$poruka=$_SESSION['poruka'];
				$pomocnik=1;
				$_SESSION['poruka']=null;
				
			}else if($_SESSION['poruka'] == "Korisnik(ci) je(su) postavljen(i) za administratora.") {
				$poruka=$_SESSION['poruka'];
				$pomocnik=1;
				$_SESSION['poruka']=null;
			}else if($_SESSION['poruka'] == "Korisnik(ci) je(su) uspešno obrisan(i).") {
				$poruka=$_SESSION['poruka'];
				$pomocnik=1;
				$_SESSION['poruka']=null;
			}else if($_SESSION['poruka'] == "Pretragu nije moguće izvršiti.") {
				$poruka=$_SESSION['poruka'];
				$pomocnik=0;
				$_SESSION['poruka']=null;
			}
		}

		$this->prikaz("pretražiKorisnike", ['poruka'=>$poruka, 'pomocnik'=>$pomocnik, 'username'=>$korisnikUsername, 'imena'=>$korisnikIme, 'prezimena'=>$korisnikPrezime, 'imePrezime'=>$korisnikImePrezime]);
    }
//---------------------------------------------------------------
	
	/**
	 * Aco Avramovic
     * Metoda koja prikazuje stranicu koja daje korisniku pregled lista koje poseduje
     *
     */
	public function pregledLista($poruka=null){
		if ((int)$poruka < 60000) {
			$imaModel= new ImaModel();
			$listaModel= new ListaModel();
			$posedujeModel= new PosedujeModel();
			$istorijaModel= new IstorijaModel();

			$imaModel->obrisiIma($poruka);
			$posedujeModel->obrisiPoseduje($poruka);
			$istorijaModel->obrisiIstorija($poruka);
			$listaModel->obrisiListu($poruka);
			$poruka=null;
		} 
			$lista= new ListaModel();
			$ima= new ImaModel();
			$sveIma= $ima->pretraga($this->session->get('korisnik')->username);
			$sveListe= [];
			$cnt=0; 
			if($sveIma != null) {
				foreach($sveIma as $red) {
					$sveListe[$cnt++]= $lista->pretragaPoIdListe($red->idLista)[0];
				}
			}
			$pomocnik=0;
			if (isset($_SESSION['poruka'])) {
				if($_SESSION['poruka'] == "Uspešno ste dodali Vašu listu.") {
					$poruka=$_SESSION['poruka'];
					$pomocnik=1;
					$_SESSION['poruka']=null;
					
				}else if($_SESSION['poruka'] == "Uspešno ste se pridruzili listi.") {
					$poruka=$_SESSION['poruka'];
					$pomocnik=1;
					$_SESSION['poruka']=null;
				}else {
					$poruka=null;
				} 
			}
		$this->prikaz('preglListe', ['poruka'=>$poruka, 'pomocnik'=>$pomocnik, 'listeKorisnika'=>$sveListe]);
	}

	/**
     * metoda za logout
     *
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
	public function logout($poruka=null) {
		$this->session->destroy();
        return redirect()->to(site_url('/'));
	}

	/**
     * Metoda koja omogucava korisniku da se uclani u neku javnu listu unosom koda
     *
     * @param $poruka- poruka o uspehu ili neuspehu
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
	public function uclani($poruka=null, $listeKorisnika=null){
		$this->prikaz('uclani', ['poruka'=>$poruka, 'listeKorisnika'=>$listeKorisnika]);
	}

	/**
	 * Aco Avramovic
     * Metoda koja omogucava korisniku da prijavi gresku
     *
     * @param $poruka- poruka o uspehu ili neuspehu
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
	public function greska($poruka=null) {
		$pomocnik=0;
		$poruka=null;
		if (isset($_SESSION['poruka'])) {
			if($_SESSION['poruka'] == "Polje poruka o greški je prazno.") {
				$poruka=$_SESSION['poruka'];
				$pomocnik=0;
				$_SESSION['poruka']=null;
				
			}else if($_SESSION['poruka'] == "Polje naslov je prazno.") {
				$poruka=$_SESSION['poruka'];
				$pomocnik=0;
				$_SESSION['poruka']=null;
			}else if($_SESSION['poruka'] == "Uspesno ste poslali prijavu.") {
				$poruka=$_SESSION['poruka'];
				$pomocnik=1;
				$_SESSION['poruka']=null;
			}else if($_SESSION['poruka'] == "Prijava greske nije uspesno poslata, pokusajte ponovo.") {
				$poruka=$_SESSION['poruka'];
				$pomocnik=0;
				$_SESSION['poruka']=null;
			}
		}
        $this->prikaz("greska", ['poruka'=>$poruka, 'pomocnik'=>$pomocnik]); 
    }

	/**
     * Metoda koja omogucava korisniku da dobije link za instalaciju ove aplikacije kada bude realizovana
     *
     * @param $poruka- poruka o uspehu ili neuspehu
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
    public function instalacijaApp($poruka=null) {
        $this->prikaz("instalacijaApp", ['poruka'=>$poruka]);
    }

	/**
	 * Aco Avramovic
     * Metoda koja omogucava korisniku da pogleda istoriju kupovina na listama koje poseduje
     *
     * @param $idListe- id liste po kojoj sistem zna za koju listu da pokaze istoriju kupovina
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
    public function istorijaKupovina($idListe=null) {
		if ($idListe != null) {
			$istorijaModel= new IstorijaModel();
			$istorija= $istorijaModel->pretragaPoIdListe($idListe);
			$bool=1;
		} else {
			$istorijaModel= new IstorijaModel();
			$imaModel= new ImaModel();
			$id=$this->session->get('korisnik')->username;
			$listeKorisnika= $imaModel->pretraga($id);
			$istorija= [];
			$cnt=0;
			if (!is_array($listeKorisnika)){
				$listeKorisnika= $imaModel->pretraga($id)[0];
				$istorija[0]=$istorijaModel->pretragaPoIdListe($listeKorisnika->idLista);
			} else {
				foreach($listeKorisnika as $lista) {
					if($istorijaModel->pretragaPoIdListe($lista->idLista) != null)
						$istorija[$cnt++]= ($istorijaModel->pretragaPoIdListe($lista->idLista));
				}
			}
			$bool=0;
	    }
        $this->prikaz("istorijaKupovina", ['istorija'=>$istorija, 'bool'=>$bool]);
    }

	/**
     * Metoda koja omogucava korisniku da ima informacije da je obrati ljudima cija je aplikacija
     *
     * @param $poruka- poruka o uspehu ili neuspehu
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
    public function kontakt($poruka=null) {
        $this->prikaz("kontakt", ['poruka'=>$poruka]);
    }

	/**
     * Metoda koja pomaze novim korisnicima da se lakse upoznaju sa aplikacijom
     *
     * @param $poruka- poruka o uspehu ili neuspehu
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
    public function pomoc($poruka=null) {
        $this->prikaz("pomoc", ['poruka'=>$poruka]);
    }

	/**
	 * Aco Avramovic
     * Metoda koja prikazuje korisniku odredjenu listu za kupovinu, trenutne namirnice koje su na njoj, budzet
     *
     * @param $poruka- poruka o uspehu ili neuspehu, $idListe- da bi se znalo o kojoj listi je rec
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
	public function listaZaKupovinu($poruka=null, $idListe=null) {
		
		$namirnice= new NamirniceModel();
		$listaNamirnica= $namirnice->sveNamirnice();
		$proizvodi= [];
		$kategorije= [];
		$cnt1=0;
		$cnt2=0;
			
		foreach($listaNamirnica as $lista) {
			$kategorije[$cnt2++]= $lista->kategorija;
		}
		$kategorije=array_unique($kategorije);
		
		$url = strtok($_SERVER['REQUEST_URI'], '?');
		$idListe = substr(strrchr($url, '/'), 1);
			
		$poseduje= new PosedujeModel();
		$spisak= $poseduje->pretragaPoIdListe($idListe);
		$niz=[];
		$cnt3=0;
		foreach($spisak as $id) {
			$niz[$cnt3++]=($namirnice->pretragaPoId($id->idNamirnica)[0]);
		}
			$poruka1=null;
			$poruka2=null;
			$poruka=null;
			$pomocnik=0;
		if (isset($_SESSION['poruka'])) {
			if($_SESSION['poruka'] == "Niste uneli namirnicu.") {
				$poruka=$_SESSION['poruka'];
				$_SESSION['poruka']=null;	
			}else if($_SESSION['poruka'] == "Polje za unos budžeta je prazno.") {
				$poruka1=$_SESSION['poruka'];
				$_SESSION['poruka']=null;
			}else if($_SESSION['poruka'] == "Uspešno unet budžet.") {
				$poruka1=$_SESSION['poruka'];
				$pomocnik=1;
				$_SESSION['poruka']=null;
			}else if($_SESSION['poruka'] == "Polje za unos računa je prazno.") {
				$poruka2=$_SESSION['poruka'];
				$_SESSION['poruka']=null;
			}else if($_SESSION['poruka'] == "Uspešno unet iznos računa.") {
				$poruka2=$_SESSION['poruka'];
				$pomocnik=1;
				$_SESSION['poruka']=null;
			}else if($_SESSION['poruka'] == "Nevalidan unos budzeta.") {
				$poruka1=$_SESSION['poruka'];
				$_SESSION['poruka']=null;
			}else if($_SESSION['poruka'] == "Nevalidan unos racuna.") {
				$poruka2=$_SESSION['poruka'];
				$_SESSION['poruka']=null;
			}else {
				$poruka=null;
			} 
		}

		$listaModel= new ListaModel();
		$lista= $listaModel->pretragaPoIdListe($idListe)[0];
        $this->prikaz("listaZaKupovinu", ['poruka'=>$poruka, 'poruka1'=>$poruka1, 'poruka2'=>$poruka2, 'pomocnik'=>$pomocnik, 'proizvodi'=>$proizvodi, 'kategorije'=>$kategorije, 'spisak'=>$niz, 'ID'=>$idListe, 'lista'=>$lista]);
    }

	/**
	 * Ivana Marjanovic
     * Metoda koja generise jedinstveni kod za javne liste pomocu kojih korisnici mogu da se pridruze listi
     *
     * @param $lenght- inicijalno je postavljeno da kod bude duzine 8 karaktera
     * @return string od 8 karaktera
     */
	public function generateRandomString($length = 8) {
		$characters = '0123456789abcdefghijklmnopqrs092u3tuvwxyzaskdhfhf9882323ABCDEFGHIJKLMNksadf9044OPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	/**
	 * Aco Avramovic
     * Metoda koja formira novu listu
     *
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
	public function napravi() {
		$imeListe= $this->request->getVar('imeListe');
		$privatnost= $this->request->getVar('privatnost');
		$kod=null;
		$lista= new ListaModel();
		$ima= new ImaModel();  

		if($privatnost == "javna") {
			$kod= $this->generateRandomString();
		}

		if($this->request->getVar('imeListe') == null) {
			return $this->napraviListu('Polje naziv liste je prazno.');
		}

		$lista->insert([ 
			'naziv'=>$imeListe,
			'privatnost'=>$privatnost,
			'kod'=>$kod,
			'budzet'=>0,
			'usernameKorisnika'=>$this->session->get('korisnik')->username
		]);
		
		$ima->insert([ 
			'idKorisnika'=>$this->session->get('korisnik')->username,
			'idLista'=>$lista->getInsertID()
		]);

		$listeKorisnika= $lista->pretragaPoUsernameKorisnika($this->session->get('korisnik')->username);
		$poruka="Uspešno ste dodali Vašu listu.";
		$_SESSION['poruka']=$poruka;
		return redirect()->to(site_url("Korisnik/pregledLista"));
	}

	/**
	 * Ivana Marjanovic
     * Metoda koja realizuje pridruzivanje korisnika na neku listu
     *
     */
	public function uclaniSe(){

		$kod= $this->request->getVar('imeListe');

		if($this->request->getVar('imeListe') == null) {
			return $this->uclani('Niste uneli kod liste.');
		}

		$lista= new ListaModel();
		$ima= new ImaModel();
		$listeKorisnika = $lista->pretragaPoKodu($kod)[0];

		$ima->insert([ 
			'idKorisnika'=>$this->session->get('korisnik')->username,
			'idLista'=>$listeKorisnika->idLista,
		]);

		//$sveIma= $ima->pretraga($this->session->get('korisnik')->username);
		//$sveListe= [];
		//$cnt=0;
		//foreach($sveIma as $red) {
		//	$sveListe[$cnt++]= $lista->pretragaPoIdListe($red->idLista)[0];
		//}
		$poruka="Uspešno ste se pridružili listi.";
		$_SESSION['poruka']=$poruka;
		return redirect()->to(site_url("Korisnik/pregledLista"));
	}

	/**
	 * Avo Avramovic
     * Metoda koja ubacuje namirnicu na listu
     *
     * @param $idListe- da bi se znalo u koju listu se ubacuje
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
	public function ubaciNamirnicu($idListe) {
		$namirnica= $this->request->getVar('namirnica');

		if($this->request->getVar('namirnica') == null) {
			$poruka="Niste uneli namirnicu.";
			$_SESSION['poruka']=$poruka;
			$poruka=null;
			return $this->listaZaKupovinu('Niste uneli namirnicu.');
		}
		$lista= new ListaModel();
		$poseduje= new PosedujeModel();
		$listaNamirnica= new NamirniceModel();


		if ($listaNamirnica->pretraga($namirnica) != null) {
			$idNamernice= $listaNamirnica->pretraga($namirnica)[0]->idNamirnice;
			$poseduje->insert([
				'idNamirnica'=>$idNamernice,
				'idLista'=>$idListe
			]);
		} else{
			$listaNamirnica->insert([
				'proizvod'=>$namirnica
			]);
			$poseduje->insert([
				'idNamirnica'=>$listaNamirnica->getInsertID(),
				'idLista'=>$idListe
			]);
		}
		$poruka=null;
		return redirect()->to(site_url("Korisnik/listaZaKupovinu/{$poruka}/{$idListe}"));
	}

	/**
	 * Aco Avramovic
     * Metoda kojom se unosi budzet za listu
     *
     * @param $idListe- da bi se znalo o kojoj listi je rec
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
	public function unesiBudzet($idListe) {

		$budzet= $this->request->getVar('budzet');
		if(!preg_match("/\d+$/", $budzet) && $budzet!='') {
			$poruka="Nevalidan unos budzeta.";
			$_SESSION['poruka']=$poruka;
			$poruka=null;
			return redirect()->to(site_url("Korisnik/listaZaKupovinu/{$poruka}/{$idListe}"));
		}
		$lista= new ListaModel();

		if($this->request->getVar('budzet') == null) {
			$poruka="Polje za unos budžeta je prazno.";
			$_SESSION['poruka']=$poruka;
			$poruka=null;
			return redirect()->to(site_url("Korisnik/listaZaKupovinu/{$poruka}/{$idListe}"));
		}

		$lista->unosBudzeta($idListe, $budzet);
		$poruka="Uspešno unet budžet.";
		$_SESSION['poruka']=$poruka;
		$poruka=null;
		return redirect()->to(site_url("Korisnik/listaZaKupovinu/{$poruka}/{$idListe}"));
	}

	/**
	 * Ivana Marjanovic
     * Metoda kojom se unosi racun i pokazuje da je izvrsena kupovina cime se stvara nova kupovina u istoriji kupovina
     *
     * @param $idListe- da bi se znalo u kojoj listi je izvrsena kupovina
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
	public function unesiRacun($idListe) {

		$racun= $this->request->getVar('racun');
		if(!preg_match("/\d+$/", $racun) && $racun!='') {
			$poruka="Nevalidan unos racuna.";
			$_SESSION['poruka']=$poruka;
			$poruka=null;
			return redirect()->to(site_url("Korisnik/listaZaKupovinu/{$poruka}/{$idListe}"));
		}
		$istorija= new IstorijaModel();
		$listaModel= new ListaModel();

		if($this->request->getVar('racun') == null) {
			$poruka="Polje za unos računa je prazno.";
			$_SESSION['poruka']=$poruka;
			$poruka=null;
			return redirect()->to(site_url("Korisnik/listaZaKupovinu/{$poruka}/{$idListe}"));
		}
		date_default_timezone_set('Europe/Belgrade');
		$datum= date_create(date('H:i, d.m.Y'));
		$istorija->insert([
			'kupac'=>$this->session->get('korisnik')->username,
			'iznosRacuna'=>$racun,
			'datum'=>date_format($datum, "Y/m/d H:i"),
			'idListe'=>$idListe
		]);
		$lista= $listaModel->pretragaPoIdListe($idListe)[0];
		$noviBudzet= $lista->budzet - $racun;
		$listaModel->unosBudzeta($idListe, $noviBudzet);
		$poruka="Uspešno unet iznos računa.";
		$_SESSION['poruka']=$poruka;
		$poruka=null;
		return redirect()->to(site_url("Korisnik/listaZaKupovinu/{$poruka}/{$idListe}"));
	}
//---------------------------------------------------------------------------------Moderator

	/**
	 * Ivana Marjanovic
     * Metoda koja realizuje dodavanje proizvoda u bazu
     *
     */
	public function dodajPredlog() {
		$proizvod=$this->request->getVar('proizvod');
		$kategorija=$this->request->getVar('kategorija');

		if($proizvod == null) {
			$poruka="Polje proizvod je prazno.";
			$_SESSION['poruka']=$poruka;
			$poruka=null;
			return redirect()->to(site_url("Korisnik/dodavanjePredloga"));
			//return $this->dodavanjePredloga('Polje proizvod je prazno.');
		}

		if($kategorija == null) {
			$poruka="Polje kategorija je prazno.";
			$_SESSION['poruka']=$poruka;
			$poruka=null;
			return redirect()->to(site_url("Korisnik/dodavanjePredloga"));
			//return $this->dodavanjePredloga('Polje kategorija je prazno.');
		}

		$namirnice= new NamirniceModel();

		$bool= $namirnice->pretraga($proizvod);
		if($bool != null) {
			$namirnice->dodajKategoriju($proizvod, $kategorija);
		} else {

			$namirnice->insert([
				'kategorija'=>$kategorija,
				'proizvod'=>$proizvod
			]);
			$poruka="Proizvod je uspešno dodat.";
			$_SESSION['poruka']=$poruka;
			$poruka=null;
			return redirect()->to(site_url("Korisnik/dodavanjePredloga"));
		}
		//return $this->dodavanjePredloga('Proizvod je uspešno dodat.');

	}

//----------------------------------------------------------------------Administrator
	/**
	 * Ivana Marjanovic
     * Metoda koja realizuje pretragu korisnika
     *
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
	public function pretraga() {

		$username=$this->request->getVar('username');
		$ime=$this->request->getVar('ime');
		$prezime=$this->request->getVar('prezime');

		$korisnik= new KorisnikModel();
		$korisnikUsername1= $korisnik->pretraga($this->request->getVar('username'));
		if($korisnikUsername1 == null) {
			$korisnikUsername=null;
		} else {
			$korisnikUsername= $korisnik->pretraga($this->request->getVar('username'))[0];
		}
		//$korisnikUsername= $korisnik->pretraga($this->request->getVar('username'))[0];
		$korisnikIme= $korisnik->pretragaIme($ime);
		$korisnikPrezime= $korisnik->pretragaPrezime($prezime);
		$korisnikImePrezime= $korisnik->pretragaImePrezime($ime, $prezime);

		if($korisnikUsername == null && $korisnikIme == null && $korisnikPrezime == null) {
			$poruka="Pretragu nije moguće izvršiti.";
			$_SESSION['poruka']=$poruka;
			$poruka=null;
			return redirect()->to(site_url("Korisnik/pretragaKorisnika"));
			//return $this->pretragaKorisnika('Polja na osnovu kojih se vrsi pretraga su prazna.');
		}

		//if($korisnikUsername == null) {
		//    return $this->pretragaKorisnika('Ne postoji korisnik pod ovim korisnickim imenom.');
		//}
		
		//if($korisnikIme == null) {
		//    return $this->pretragaKorisnika('Ne postoji korisnik pod ovim imenom.');
		//}
		
		//if($korisnikPrezime == null) {
		//    return $this->pretragaKorisnika('Ne postoji korisnik pod ovim prezimenom.');
		//}
		
		
		return $this->pretragaKorisnika(null, $korisnikUsername, $korisnikIme, $korisnikPrezime, $korisnikImePrezime);

	}

	/**
	 * Aco Avramovic
     * Metoda koja realizuje brisanje korisnika
     *
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
	public function ukloni() {
		$brojac=1;
		$korisnikModel= new KorisnikModel();
		$imaModel= new ImaModel();
		$listaModel= new ListaModel();
		$istorijaModel= new IstorijaModel();
		$posedujeModel= new PosedujeModel();
		while($brojac<20) { 

			if($this->request->getVar($brojac) != null) {
				$username= $this->request->getVar('ident'.$brojac);
				
				$imaModel->obrisiKorisnika($username);
			
				if (is_array($listaModel->pretragaPoUsernameKorisnika($username))){
					$liste= $listaModel->pretragaPoUsernameKorisnika($username);
					foreach($liste as $lista) {
						if(($imaModel->pretragaPoIdListe($lista->idLista)) == null){
							
							if($posedujeModel->pretragaPoIdListe($lista->idLista)){
								$posedujeModel->obrisiPoseduje($lista->idLista);
							}
							$listaModel->obrisiListu($lista->idLista);
							$korisnikModel->obrisiKorisnika($username);
							if($istorijaModel->pretragaPoIdListe($lista->idLista)) {
								$istorijaModel->obrisiIstorija($lista->idLista);
							}
							
						}
					}
				} else {
					$liste= $listaModel->pretragaPoUsernameKorisnika($username)[0];
					if(($imaModel->pretragaPoIdListe($liste->idLista)) == null){
							
						if($posedujeModel->pretragaPoIdListe($liste->idLista)){
							$posedujeModel->obrisiPoseduje($liste->idLista);
						}
						$listaModel->obrisiListu($liste->idLista);
							$korisnikModel->obrisiKorisnika($username);
						if($istorijaModel->pretragaPoIdListe($liste->idLista)) {
							$istorijaModel->obrisiIstorija($liste->idLista);
						}
						
					}
				}
				  
			}
			$brojac++;
		}
		$poruka="Korisnik(ci) je(su) uspešno obrisan(i).";
		$_SESSION['poruka']=$poruka;
		$poruka=null;
		return redirect()->to(site_url('Korisnik/pretragaKorisnika'));
	}

	/**
	 * Ivana Marjanovic
     * Metoda koja realizuje postavljanje korisnika za Administratora
     *
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
	public function postaviZaAdministratora() {
		$brojac=1;
		$korisnik= new KorisnikModel();
		while($brojac<20) {

			if($this->request->getVar($brojac) != null) {
				$korisnik->promenaRole($this->request->getVar('ident'.$brojac), 'admin');  
			}
			$brojac++;
		}
		$poruka="Korisnik(ci) je(su) postavljen(i) za administratora.";
		$_SESSION['poruka']=$poruka;
		$poruka=null;
		return redirect()->to(site_url('Korisnik/pretragaKorisnika'));
	}

	/**
	 * Ivana Marjanovic
     * Metoda koja reaizuje postavljanje korisnika za moeratora
     *
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
	public function postaviZaModeratora() {
		$brojac=1;
		$korisnik= new KorisnikModel();
		while($brojac<20) {

			if($this->request->getVar($brojac) != null) {
				$korisnik->promenaRole($this->request->getVar('ident'.$brojac), 'admin');  
			}
			$brojac++;
		}
		$poruka="Korisnik(ci) je(su) postavljen(i) za moderatora.";
		$_SESSION['poruka']=$poruka;
		$poruka=null;
		return redirect()->to(site_url('Korisnik/pretragaKorisnika'));
	}

	/**
	 * Aco Avramovic
     * Metoda koja salje mejl u kojoj se nalazi nslov i poruka o greski koju je korisnik prijavio
     *
     * @return \CodeIgniter\HTTP\RedirectResponse
     */
	public function prijaviGresku() {

		$naslov= $this->request->getVar('naslov');
		$poruka= $this->request->getVar('poruka');
		$emailKorisnika= $this->session->get('korisnik')->email;
		$username= $this->session->get('korisnik')->username;
		$brTelefona= $this->session->get('korisnik')->brTelefona;

		if ($naslov == null) {
			$poruka="Polje naslov je prazno.";
			$_SESSION['poruka']=$poruka;
			$poruka=null;
			return redirect()->to(site_url("Korisnik/greska"));
			//return $this->greska('Polje naslov je prazno.');
		}
		if ($poruka == null) {
			$poruka="Polje poruka o greški je prazno.";
			$_SESSION['poruka']=$poruka;
			$poruka=null;
			return redirect()->to(site_url("Korisnik/greska"));
			//return $this->greska('Polje poruka o greški je prazno.');
		}

		$to = "acoavramovic02@gmail.com";
		
		$email = \Config\Services::email();

		$email->setTo($to);
		$email->setFrom("unicartaplikacija@gmail.com","Prijava greske");
		$email->setSubject($emailKorisnika);

		$message ="<pre>Korisnicko ime posiljaoca: ".$username ."<br>Email posiljaoca: ".$emailKorisnika;
		if(!empty($brTelefona)){
			$message = $message." <br>Telefon: ".$brTelefona;
		}

		$message = $message."<br>Sadrzaj poruke: <br>".$poruka;

		$message = $message."</pre>";
		$email->setMessage($message);

		if($email->send()){
			$poruka="Uspesno ste poslali prijavu.";
			$_SESSION['poruka']=$poruka;
			$poruka=null;
			return redirect()->to(site_url("Korisnik/greska"));
			//return $this->greska('Uspesno ste poslali prijavu.');
		}
		else{
			$poruka="Prijava greske nije uspesno poslata, pokusajte ponovo.";
			$_SESSION['poruka']=$poruka;
			$poruka=null;
			return redirect()->to(site_url("Korisnik/greska"));
			//return $this->greska('Prijava greske nije uspesno poslata, pokusajte ponovo.');
		}

	}

	/**
	 * Ivana Marjanovic
     * Metoda koja realizuje padajuce liste na stranici listaZaKupovinu
     *
     */
	function akcija() {

		if($this->request->getVar('zahtev')){
			$kategorija= $this->request->getVar('zahtev');

			$namirniceModel= new NamirniceModel();
			$proizvodi= $namirniceModel->pretragaPoKategoriji($this->request->getVar('zahtev'));

			echo json_encode($proizvodi);
		}

	}

	/**
	 * Aco Avramovic
     * Metoda kojom se ubacuju namirnice preko padajuce liste
     *
     */
	function ubaci2() {

		if($this->request->getVar('proizvod')){
			$proizvod= $this->request->getVar('proizvod');
			$idListe= $this->request->getVar('id');

			$namirniceModel= new NamirniceModel();
			$posedujeModel= new PosedujeModel();
			$proizvodi= $namirniceModel->pretragaPoProizvodu($proizvod)[0];

			$posedujeModel->insert([
				'idNamirnica'=>$proizvodi->idNamirnice,
				'idLista'=>$idListe
			]);

			echo json_encode($proizvodi);
		}

	}

	/**
	 * Aco Avramovic
     * Metoda kojom se brisu namirnice iz liste
     *
     */
	function izbaciNamirnicu() {

		if($this->request->getVar('idnamirnice')){
			$idnamirnice= $this->request->getVar('idnamirnice');
			$idListe= $this->request->getVar('id');

			$namirniceModel= new NamirniceModel();
			$posedujeModel= new PosedujeModel();
			
			$posedujeModel->obrisiPoNamirniciPoListi($idListe, $idnamirnice);
			
			$proizvodi= $posedujeModel->pretragaPoIdListe($idListe);

			$niz=[];
			$cnt=0;
			foreach($proizvodi as $proizvod) {
				$niz[$cnt++]=($namirniceModel->pretragaPoId($proizvod->idNamirnica)[0]);
			}

			echo json_encode($niz);
		}

	}
}