<!DOCTYPE html>
<html>
<head>
<title>UniCart - Pretraga</title>
<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
    <link rel="shortcut icon" href="http://localhost/projekat/slike/iconCart.png"/>  
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
h2 { font-family: Candara, Arial, Helvetica, sans-serif; font-size: 32px; margin:10px; color: #f2f2f2 }
a {transition: 15s;}

body {
  margin: 10px auto;
  font-family: 'Lato';
  background: url("http://localhost/projekat/slike/black.jpg");
  height: 100%;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  color:#fff;
}

input[type=text], select {
  width: 60%;
  padding: 12px 20px;
  margin: 8px 8px;
  display: block;
  border: 1px solid #ccc;
  border-radius: 15px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 100%;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 5px;
  border: none;
  border-radius: 15px;
  cursor: pointer;
  align-items: center;
}
#pretrazi {
  width: 50%;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 5px;
  border: none;
  border-radius: 15px;
  cursor: pointer;
  align-items: center;
}

input[type=submit]:hover {
  background-color: #45a049;
}

div.container {
  background-color:#8da4aa;
  color:black;
  width:25%;
  border-radius: 15px;
  padding: 20px;
  text-align: center;
  
}

div.container2 {
  background-color:#8da4aa;
  color:black;
  height:90%;
  border-radius: 15px;
  padding: 20px;
  text-align: center;
  
}

.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #111;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

#main {
  transition: margin-left .5s;
  padding: 16px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

.topnav {
  overflow: hidden;
  background-color: #8da4aa;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  opacity:0.6;
  color: black;
}

.topnav a.active {
  background-color: #34c2a8;
  color: white;
}
.topnav-left {
  float: left;
}

.topnav-right {
  float: right;
}
.dropdown {
  float: left;
  overflow: hidden;
}

.dropdown .dropbtn {
  font-size: 16px;  
  border: none;
  outline: none;
  color: white;
  padding: 14px 16px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;
}
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {
  background-color: #ddd;
}

.dropdown:hover .dropdown-content {
  display: block;
}

#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border-bottom: 1px solid rgb(10, 10, 10);
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #959b95;
  color: rgb(14, 11, 11);
}

#tabelaKorisnika{
    float: right;
}

#raspored{
    display: flex;
  flex-direction: row;
  justify-content: space-around;
}

#dugmad{ 
  display: flex;
  flex-direction: row;
  justify-content:space-around;
}
</style>
</head>
<body>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
}
</script>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <?= anchor("$controller/napraviListu", "Napravi novu listu") ?>
  <p></p>
  <p style=color:gray><small>   Već imaš kod?</small></p>
  <?= anchor("$controller/uclani", "Pridruži se") ?>
  <?= anchor("$controller/pregledLista", "Pregledaj liste") ?>
  <?= anchor("$controller/kontakt", "Kontaktirajte nas") ?>
  <?= anchor("$controller/instalacijaApp", "Instaliraj našu aplikaciju") ?>
  <?= anchor("$controller/istorijaKupovina", "Istorija kupovina") ?>
  <?php
    if($korisnik->rola == "admin") {
     echo anchor("$controller/pretragaKorisnika", "Pretraži korisnike");
    } ?>
  <?php
    if($korisnik->rola == "moderator") {
     echo anchor("$controller/dodavanjePredloga", "Dodavanje predloga");
    } ?>
  <?= anchor("$controller/logout", "Izloguj se") ?>
</div>

<div id="main">
  <span style="color:white;font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Meni</span>
  <span style="color: white;"> &nbsp;Autor: <?php  echo $korisnik->ime." ".$korisnik->prezime." "; ?> </span>
</div>

<div class="topnav">
<?= anchor("$controller/pocetna", "Početna") ?>
  
  
  <div class="topnav-right">
      <?= anchor("$controller/pomoc", "Pomoć") ?>
      <?= anchor("$controller/greska", "Prijavi grešku") ?>
  </div>

</div>

<center><h2>Pretraga korisnika sajta</h2>
<center><?php
       if(isset($poruka)) {
        if ($pomocnik==0){
          echo "<font color='red'>$poruka</font><br>"; 
        } else {
          echo "<font color='green'>$poruka</font><br>";
        }
      }
  ?></center>
<p> &nbsp;</p>
</center>
<div id="raspored">
<div class="container">
  <form method="POST" action="<?= site_url("$controller/pretraga") ?>">
    <label for="username">Usename korisnika:</label>
   <center> <input type="text" id="ime" name="username" ></center>

    <label for="ime">Ime korisnika:</label>
    <center><input type="text" id="ime" name="ime"></center>

    <label for="prezime">Prezime korinsika:</label>
    <center><input type="text" id="prezime" name="prezime"></center>

    <input type="submit" value="Pretraži" id="pretrazi">

  </form>

</div>
<div>
  <div class="container2">
  <table id="customers">
    <tr>
      <th>da/ne</th>
      <th>Username</th>
      <th>Password</th>
      <th>Uloga</th>
      <th>Ime</th>
      <th>Prezime</th>
      <th>Datum rođenja</th>
      <th>E-mail</th>
    </tr>

    <?php
      $action1 = site_url("$controller/ukloni");
      $action2 = site_url("$controller/postaviZaAdministratora");
      $action3 = site_url("$controller/postaviZaModeratora");
      echo "<form method = 'post' name='form1' ac1='$action1' ac2='$action2' ac3='$action3' id='form1' action='$action3'>";
      if($username != null) {
        $cnt=1;
        echo "<tr>
        <td><input type='checkbox' name='1'></td>
        <td name='u{$cnt}'>{$username->username}</td>
        <td>{$username->password}</td>
        <td>{$username->rola}</td>
        <td>{$username->ime}</td>
        <td>{$username->prezime}</td>
        <td>{$username->datumRodjenja}</td>
        <td>{$username->email}</td>
        <td><input type='hidden' name='ident' value='$username->username'></td>
        </tr>";
      } else if($imePrezime!=null) {
        $cnt=1;
        foreach ($imePrezime as $imePrezime) {
          echo "<tr>
          <td><input type='checkbox' name='{$cnt}'></td>
          <td name='u{$cnt}'>{$imePrezime->username}</td>
          <td>{$imePrezime->password}</td>
          <td>{$imePrezime->rola}</td>
          <td>{$imePrezime->ime}</td>
          <td>{$imePrezime->prezime}</td>
          <td>{$imePrezime->datumRodjenja}</td>
          <td>{$imePrezime->email}</td>
          <td><input type='hidden' name='ident{$cnt}' value='$imePrezime->username'></td>
          </tr>";
          $cnt++;
        }
      } else if($imena!= null && $prezimena==null) {
        $cnt=1;
        foreach ($imena as $ime) {
          echo "<tr>
          <td><input type='checkbox' name='{$cnt}'></td>
          <td name='u{$cnt}'>{$ime->username}</td>
          <td>{$ime->password}</td>
          <td>{$ime->rola}</td>
          <td>{$ime->ime}</td>
          <td>{$ime->prezime}</td>
          <td>{$ime->datumRodjenja}</td>
          <td>{$ime->email}</td>
          <td><input type='hidden' name='ident{$cnt}' value='$ime->username'></td>
          </tr>";
          $cnt++;
        }
      } else if($prezimena!= null && $imena==null) {
        $cnt=1;
        foreach ($prezimena as $prezime) {
          echo "<tr>
          <td><input type='checkbox' name='{$cnt}'></td>
          <td name='u{$cnt}'>{$prezime->username}</td>
          <td>{$prezime->password}</td>
          <td>{$prezime->rola}</td>
          <td>{$prezime->ime}</td>
          <td>{$prezime->prezime}</td>
          <td>{$prezime->datumRodjenja}</td>
          <td>{$prezime->email}</td>
          <td><input type='hidden' name='ident{$cnt}' value='$prezime->username'></td>
          </tr>";
          $cnt++;
        }
      }
      echo "</form>";
    ?>
  </table>
</div>
  <div id="dugmad">
   
  <input type="submit" class="dugmici" value="Ukloni iz baze" id="button1">
  <input type="submit" class="dugmici" value="Postavi za administratora" id="button2">
  <input type="submit" class="dugmici" value="Postavi za moderatora" id="button3">
  </div>
</div>
</div>

<script>
  $("#button1").click(() => {
    $("#form1").attr("action",$("#form1").attr("ac1"));
    document.forms['form1'].submit()
  });
  $("#button2").click(() => {
    $("#form1").attr("action",$("#form1").attr("ac2"));
    document.forms['form1'].submit()
  });
  $("#button3").click(() => {
    $("#form1").attr("action",$("#form1").attr("ac3"));
    document.forms['form1'].submit()
  });

</script>


</body>
</html>