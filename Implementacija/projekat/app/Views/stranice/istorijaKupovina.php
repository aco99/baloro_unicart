<!DOCTYPE html>
<html>
<head>
<title>UniCart - Istorija kupovina</title>
    <link rel="shortcut icon" href="http://localhost/projekat/slike/iconCart.png"/>   
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
h2 { font-family: Candara, Arial, Helvetica, sans-serif; font-size: 32px; margin:10px; color: #f2f2f2 }
a {transition: 15s;}

body {
  margin: 10px auto;
  font-family: 'Lato';
  background: url("http://localhost/projekat/slike/black.jpg");
  height: 650px;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  color:#fff;
}

input[type=text], select {
  width: 50%;
  padding: 12px 20px;
  margin: 8px 0;
  display: block;
  border: 1px solid #ccc;
  border-radius: 15px;
  box-sizing: border-box;
}
#poruka {
  width: 50%;
  padding: 12px 20px;
  margin: 8px 0;
  display: block;
  border: 1px solid #ccc;
  border-radius: 15px;
  box-sizing: border-box;
}
input[type=mail], select {
  width: 50%;
  padding: 12px 20px;
  margin: 8px 0;
  display: block;
  border: 1px solid #ccc;
  border-radius: 15px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 50%;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 15px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

div.container {
  background-color:#8da4aa;
  color:black;
  width:50%;
  border-radius: 15px;
  padding: 20px;
}


.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #111;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

#main {
  transition: margin-left .5s;
  padding: 16px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}

.topnav {
  overflow: hidden;
  background-color: #8da4aa;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  opacity:0.6;
  color: black;
}

.topnav a.active {
  background-color: #34c2a8;
  color: white;
}
.topnav-left {
  float: left;
}

.topnav-right {
  float: right;
}
.dropdown {
  float: left;
  overflow: hidden;
}

.dropdown .dropbtn {
  font-size: 16px;  
  border: none;
  outline: none;
  color: white;
  padding: 14px 16px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;
}
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {
  background-color: #ddd;
}

.dropdown:hover .dropdown-content {
  display: block;
}

#prijavigresku {
  font-size: 25px;
}

#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border-bottom: 1px solid rgb(10, 10, 10);
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #959b95;
  color: rgb(14, 11, 11);
}

</style>
</head>
<body>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
}
</script>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <?= anchor("$controller/napraviListu", "Napravi novu listu") ?>
  <p></p>
  <p style=color:gray><small>   Već imaš kod?</small></p>
  <?= anchor("$controller/uclani", "Pridruži se") ?>
  <?= anchor("$controller/pregledLista", "Pregledaj liste") ?>
  <?= anchor("$controller/kontakt", "Kontaktirajte nas") ?>
  <?= anchor("$controller/instalacijaApp", "Instaliraj našu aplikaciju") ?>
  <?= anchor("$controller/istorijaKupovina", "Istorija kupovina") ?>
  <?php
    if($korisnik->rola == "admin") {
     echo anchor("$controller/pretragaKorisnika", "Pretraži korisnike");
    } ?>
  <?php
    if($korisnik->rola == "moderator") {
     echo anchor("$controller/dodavanjePredloga", "Dodavanje predloga");
    } ?>
  <?= anchor("$controller/logout", "Izloguj se") ?>
</div>

<div id="main">
  <span style="color:white;font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Meni</span>
  <span style="color: white;"> &nbsp;Autor: <?php  echo $korisnik->ime." ".$korisnik->prezime." "; ?> </span>
</div>

<div class="topnav">
<?= anchor("$controller/pocetna", "Početna") ?>
  
  
  <div class="topnav-right">
      <?= anchor("$controller/pomoc", "Pomoć") ?>
      <?= anchor("$controller/greska", "Prijavi grešku") ?>
  </div>

</div>

<center>
<p id="prijavigresku">Istorija kupovina:</p></center>
<center><?php
       if(isset($poruka)) echo "<font color='red'>$poruka</font><br>"; 
  ?></center>
<p> &nbsp;</p>
<center><div class="container">
    <table id='customers'>
        <tr>
          <th>Ime kupca:</th>
          <th>Iznos računa:</th>
          <th>Datum i vreme:</th>
        </tr>
        <?php
        if ($bool==0) {
          if($istorija != null){
            foreach ($istorija as $kupovina) {
              if($kupovina != null) {
                if(is_array($kupovina)) {
                  foreach ($kupovina as $kup) {
                    echo "<tr>
                    <td>{$kup->kupac}</td>
                    <td>{$kup->iznosRacuna}</td>
                    <td>{$kup->datum}</td>
                    </tr>";
                  }
                } else {
                  echo "<tr>
                  <td>{$kupovina->kupac}</td>
                  <td>{$kupovina->iznosRacuna}</td>
                  <td>{$kupovina->datum}</td>
                  </tr>";
                }
              }
            }
          }
        }else {
          if(count($istorija) > 0) {
            foreach ($istorija as $kup) {
              echo "<tr>
              <td>{$kup->kupac}</td>
              <td>{$kup->iznosRacuna}</td>
              <td>{$kup->datum}</td>
              </tr>";
            }
          }
        }
        ?>
    </table>
</div></center>

</body>
</html>