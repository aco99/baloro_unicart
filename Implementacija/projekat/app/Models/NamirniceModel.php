<?php namespace App\Models;

use CodeIgniter\Model;

/**
* Aco Avramovic
* NamirinceModel – klasa koja predstavlja tabelu namirnice u bazi
*
* @version 1.0
*/

class NamirniceModel extends Model
{
        protected $table      = 'namirnice';
        protected $primaryKey = 'idNamirnice';
        protected $returnType = 'object';
        protected $allowedFields = ['idNamirnice', 'kategorija', 'proizvod'];

        public function pretraga($proizvod) {
            return $this->where('proizvod', $proizvod)->findAll();
        }

        public function pretragaPoProizvodu($proizvod) {
            return $this->where('proizvod', $proizvod)->findAll();
        }

        public function pretragaPoKategoriji($kategorija) {
            return $this->where('kategorija', $kategorija)->findAll();
        }

        public function pretragaPoId($id) {
            return $this->where('idNamirnice', $id)->findAll();
        }

        public function sveNamirnice() {
            return $this->findAll();
        }
    
        public function dodajKategoriju($proizvod, $kategorija) {
            $this->where('proizvod', $proizvod)->set([
                'kategorija'=>$kategorija 
            ])->update();
        }
}