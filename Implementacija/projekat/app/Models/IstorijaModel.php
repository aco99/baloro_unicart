<?php namespace App\Models;

use CodeIgniter\Model;

/**
* Aco Avramovic
* IstorijaModel – klasa koja predstavlja tabelu istorijakupovina u bazi
*
* @version 1.0
*/

class IstorijaModel extends Model
{
        protected $table      = 'istorijakupovina';
        protected $primaryKey = 'idKupovine';
        protected $returnType = 'object';
        protected $allowedFields = ['idKupovine', 'kupac', 'iznosRacuna', 'datum', 'idListe'];

        public function pretraga($idKupovine) {
            return $this->where('idKupovine', $idKupovine)->findAll();
        }

        public function pretragaPoIdListe($idLista) {
            return $this->where('idListe', $idLista)->findAll();
        }

        public function obrisiIstorija($id) {
            $this->where('idListe', $id)->delete();
        }

}