<?php namespace App\Models;

use CodeIgniter\Model;

/**
* Ivana Marjanovic
* ListaModel – klasa koja predstavlja tabelu lista u bazi
*
* @version 1.0
*/

class ListaModel extends Model
{
        protected $table      = 'lista';
        protected $primaryKey = 'idLista';
        protected $returnType = 'object';
        protected $allowedFields = ['idLista', 'naziv', 'privatnost', 'kod', 'budzet', 'usernameKorisnika'];

        public function pretragaPoKodu($kod) {
            return $this->where('kod', $kod)->findAll();
        }

        public function pretragaPoIdListe($idLista) {
            return $this->where('idLista', $idLista)->findAll();
        }

        public function pretragaPoUsernameKorisnika($username) {
            return $this->where('usernameKorisnika', $username)->findAll();
        }

        public function unosBudzeta($id, $budzet) {
            $this->where('idLista', $id)->set([
                'budzet'=>$budzet
            ])->update();
        }

        public function obrisiListu($id) {
            $this->where('idLista', $id)->delete();
        }
    
}