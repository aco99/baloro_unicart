<?php namespace App\Models;

use CodeIgniter\Model;

/**
* Aco Avramovic
* KorisnikModel – klasa koja predstavlja tabelu korisnik u bazi
*
* @version 1.0
*/

class KorisnikModel extends Model
{
        protected $table      = 'korisnik';
        protected $primaryKey = 'username';
        protected $returnType = 'object';
        protected $allowedFields = ['username', 'password', 'ime', 'prezime', 'datumRodjenja', 'brTelefona', 'email', 'rola'];

        public function pretraga($username) {
            return $this->where('username', $username)->findAll();
        }

        public function pretragaIme($ime) {
            return $this->where('ime', $ime)->findAll();
        }

        public function pretragaPrezime($prezime) {
            return $this->where('prezime', $prezime)->findAll();
        }

        public function pretragaImePrezime($ime, $prezime) {
            return $this->where('ime', $ime)->where('prezime', $prezime)->findAll();
        }

        public function ispravanPassword($password) {
            return $this->where('password', $password)->findAll();
        }

        public function obrisiKorisnika($id) {
            $this->where('username', $id)->delete();
        }

        public function promenaRole($id, $rola) {
            $this->where('username', $id)->set([
                'rola'=>$rola
            ])->update();
        }

    
}