<?php namespace App\Models;

use CodeIgniter\Model;

/**
 * Ivana Marjanovic
* ImaModel – klasa koja predstavlja tabelu ima u bazi
*
* @version 1.0
*/

class ImaModel extends Model
{
        protected $table      = 'ima';
        protected $primaryKey = 'id';
        protected $returnType = 'object';
        protected $allowedFields = ['id', 'idKorisnika', 'idLista'];

        public function pretraga($idKorisnika) {
            return $this->where('idKorisnika', $idKorisnika)->findAll();
        }

        public function pretragaPoIdListe($idLista) {
            return $this->where('idLista', $idLista)->findAll();
        }
    
        public function obrisiIma($id) {
            $this->where('idLista', $id)->delete();
        }

        public function obrisiKorisnika($idKorisnika) {
            $this->where('idKorisnika', $idKorisnika)->delete();
        }
       
}