<?php namespace App\Models;

use CodeIgniter\Model;

/**
* Aco Avramovic
* PosedujeModel – klasa koja predstavlja tabelu poseduje u bazi
*
* @version 1.0
*/

class PosedujeModel extends Model
{
        protected $table      = 'poseduje';
        protected $primaryKey = 'id';
        protected $returnType = 'object';
        protected $allowedFields = ['id', 'idNamirnica', 'idLista'];

        public function pretraga($idNamernice) {
            return $this->where('idNamirnica', $idNamernice)->findAll();
        }

        public function pretragaPoIdListe($idLista) {
            return $this->where('idLista', $idLista)->findAll();
        }
    
        public function obrisiPoseduje($id) {
            $this->where('idLista', $id)->delete();
        }

        public function obrisiPoNamirniciPoListi($id, $idNamirnica) {
            $this->where('idLista', $id)->where('idNamirnica', $idNamirnica)->delete();
        }
}